package com.example.tickettypedetection;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText number;
    Button btn;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result = findViewById(R.id.result);
        btn = findViewById(R.id.button);
        number = findViewById(R.id.editTextNumber);
        Algoritm algoritm = new Algoritm();

        algoritm.setMainActivity(this);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int numb = Integer.parseInt(number.getText().toString());

                    if(algoritm.getResult(numb)) {
                        result.setText("Число счастливое");
                    } else{
                        result.setText("Число не счастливое");
                    }
                } catch (Exception e){
                    Toast.makeText(MainActivity.this, "Пустая строка", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}